# Changes

## 0.3.1 (2025-01-21)

### Fix

- bump default publish-to-pypi Python version

## 0.3.0 (2025-01-15)

### Feat

- enable PEP-740 attestations when publishing to Pypi

## 0.2.0 (2024-10-18)

### Feat

- add script-aliases component

## 0.1.2 (2024-09-06)

### Fix

- set commitizen image entrypoint

## 0.1.1 (2024-09-06)

### Fix

- fix commitizen post_bump_hook

## 0.1.0 (2024-09-06)

### Feat

- use commitizen to generate Gitlab release notes

## 0.0.1 (2024-05-06)

- Initial Release
