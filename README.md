# thelab-ci-components

This repository contains [reusable CI components](https://docs.gitlab.com/ee/ci/components/) for our open-source projects.

## Components

### precommit

Checks [pre-commit](https://pre-commit.com/) hooks.

```yml
include:
  - component: gitlab.com/thelabnyc/thelab-ci-components/precommit@<VERSION>
```

| Input               | Default                               | Description                      |
| ------------------- | ------------------------------------- | -------------------------------- |
| stage               | `release`                             | Gitlab CI stage.                 |
| image               | `python:3`                            | Docker image to use.             |
| precommit_cache_dir | `${CI_PROJECT_DIR}/.cache/pre-commit` | Pre-commit hook cache directory. |

### publish-gitlab-release

Publish a Gitlab release.

```yml
include:
  - component: gitlab.com/thelabnyc/thelab-ci-components/publish-gitlab-release@<VERSION>
```

| Input | Default   | Description      |
| ----- | --------- | ---------------- |
| stage | `release` | Gitlab CI stage. |

### publish-to-pypi

Publish a Python package to PyPI. Authentication is handled using PyPI's [trusted publisher system](https://docs.pypi.org/trusted-publishers/using-a-publisher/).

```yml
include:
  - component: gitlab.com/thelabnyc/thelab-ci-components/publish-to-pypi@<VERSION>
    inputs:
      pkg_dir: "./src/my-package"
```

| Input       | Default                                      | Description                                    |
| ----------- | -------------------------------------------- | ---------------------------------------------- |
| stage       | `release`                                    | Gitlab CI stage.                               |
| environment | `release`                                    | Gitlab CI environment name.                    |
| image       | `registry.gitlab.com/thelabnyc/python:py311` | Docker image to use during publishing.         |
| pkg_dir     | `$CI_PROJECT_DIR`                            | Report directory where `pyproject.toml` lives. |

### script-aliases

Commonly used script chunks that can be `!reference`'d as-needed.

```yml
include:
  - component: gitlab.com/thelabnyc/thelab-ci-components/script-aliases@<VERSION>

deploy-or-whatever:
  script:
    - !reference [.install-awscli, script]
```
